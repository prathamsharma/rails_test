Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root 'health_regions#index'

  resources :health_regions
  get 'import_data_api', to: 'health_regions#import_data_api'
end
