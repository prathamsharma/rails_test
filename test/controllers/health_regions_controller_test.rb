require 'test_helper'

class HealthRegionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @health_region = health_regions(:one)
  end

  test "should get index" do
    get health_regions_url
    assert_response :success
  end

  test "should get new" do
    get new_health_region_url
    assert_response :success
  end

  test "should create health_region" do
    assert_difference('HealthRegion.count') do
      post health_regions_url, params: { health_region: {  } }
    end

    assert_redirected_to health_region_url(HealthRegion.last)
  end

  test "should show health_region" do
    get health_region_url(@health_region)
    assert_response :success
  end

  test "should get edit" do
    get edit_health_region_url(@health_region)
    assert_response :success
  end

  test "should update health_region" do
    patch health_region_url(@health_region), params: { health_region: {  } }
    assert_redirected_to health_region_url(@health_region)
  end

  test "should destroy health_region" do
    assert_difference('HealthRegion.count', -1) do
      delete health_region_url(@health_region)
    end

    assert_redirected_to health_regions_url
  end
end
