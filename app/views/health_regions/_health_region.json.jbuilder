json.extract! health_region, :id, :created_at, :updated_at
json.url health_region_url(health_region, format: :json)
