class HealthRegion < ApplicationRecord
  def self.to_csv
  attributes = %w{dimension british_columbia ha_total ha_1_interior ha_2_fraser ha_3_vancouver_coastal ha_4_vancouver_island ha_5_northern}

    CSV.generate(headers: true) do |csv|
      csv << attributes

      all.each do |user|
        csv << attributes.map{ |attr| user.send(attr) }
      end
    end
  end

  def self.save_health_region_data(records)
    self.transaction do
      health_region = []
      records.each do |record|
        health_region << self.new(dimension: record["Dimension"] , british_columbia: record["British Columbia"] , 
          ha_total: record["HA Total"] , ha_1_interior: record["HA 1 Interior"] , 
          ha_2_fraser: record["HA 2 Fraser"] , ha_3_vancouver_coastal: record["HA 3 Vancouver Coastal"], 
          ha_4_vancouver_island: record["HA 4 Vancouver Island"], ha_5_northern: record["HA 5 Northern"])
      end
      self.import health_region
    end
  end

end
