class HealthRegionsController < ApplicationController
  before_action :set_health_region, only: [:show, :edit, :update, :destroy]

  # GET /health_regions
  # GET /health_regions.json
  def index
    @health_regions = HealthRegion.all
    respond_to do |format|
      format.html
      format.csv { send_data @health_regions.to_csv, filename: "health_regions-#{Date.today}.csv" }
    end
  end

  # GET /health_regions/1
  # GET /health_regions/1.json
  def show
  end

  # GET /health_regions/new
  def new
    @health_region = HealthRegion.new
  end

  # GET /health_regions/1/edit
  def edit
  end

  # POST /health_regions
  # POST /health_regions.json
  def create
    @health_region = HealthRegion.new(health_region_params)

    respond_to do |format|
      if @health_region.save
        format.html { redirect_to @health_region, notice: 'Health region was successfully created.' }
        format.json { render :show, status: :created, location: @health_region }
      else
        format.html { render :new }
        format.json { render json: @health_region.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /health_regions/1
  # PATCH/PUT /health_regions/1.json
  def update
    respond_to do |format|
      if @health_region.update(health_region_params)
        format.html { redirect_to @health_region, notice: 'Health region was successfully updated.' }
        format.json { render :show, status: :ok, location: @health_region }
      else
        format.html { render :edit }
        format.json { render json: @health_region.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /health_regions/1
  # DELETE /health_regions/1.json
  def destroy
    @health_region.destroy
    respond_to do |format|
      format.html { redirect_to health_regions_url, notice: 'Health region was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def import_data_api
    find_offset = HealthRegion.count.to_s
    res = GetHealthRegionDataService.get_health_region_data(find_offset, params[:limit])
    if res.present?
      HealthRegion.save_health_region_data(res)
      redirect_to health_regions_path, notice: 'Health region was imported successfully.'
    else
      redirect_to health_regions_path, notice: 'Please try again.'
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_health_region
      @health_region = HealthRegion.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def health_region_params
      params.require(:health_region).permit(:dimension, :british_columbia, :ha_total, :ha_1_interior, :ha_2_fraser, :ha_3_vancouver_coastal, :ha_4_vancouver_island, :ha_5_northern)
    end
end
