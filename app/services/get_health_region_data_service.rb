class GetHealthRegionDataService
    BASE_URL = "https://catalogue.data.gov.bc.ca"

    def self.get_health_region_data(offset, limit)
        res = Faraday.get "#{BASE_URL}/api/3/action/datastore_search?resource_id=1665c496-7e16-487a-bb6a-54f05d166bc7&limit=#{limit}&offset=#{offset}"
        JSON.parse(res.body)["result"]["records"] rescue []
    end
end