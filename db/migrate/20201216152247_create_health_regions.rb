class CreateHealthRegions < ActiveRecord::Migration[5.0]
  def change
    create_table :health_regions do |t|
      t.string :dimension
      t.string :british_columbia
      t.string :ha_total
      t.string :ha_1_interior
      t.string :ha_2_fraser
      t.string :ha_3_vancouver_coastal
      t.string :ha_4_vancouver_island
      t.string :ha_5_northern
      t.timestamps
    end
  end
end
